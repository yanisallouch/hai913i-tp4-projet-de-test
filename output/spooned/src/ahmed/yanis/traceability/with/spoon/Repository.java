package ahmed.yanis.traceability.with.spoon;
import exception.ProductAlreadyExisteException;
import exception.ProductNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
public class Repository {
    private static Logger LOGGER = Logger.getLogger(Repository.class.getName());

    private FileHandler fh;

    private IOException e1638976231000;

    private String ID;

    private List<Product> products;

    public Repository() {
        super();
        this.products = new ArrayList<>();
        try { fh = new FileHandler("Repository.log",true); LOGGER.addHandler(fh); } catch (SecurityException e) { LOGGER.severe("Impossible to open FileHandler"); }catch (IOException e) { LOGGER.severe("Impossible to open FileHandler"); };
    }

    public Repository(String iD, List<Product> products) {
        super();
        ID = iD;
        this.products = products;
        try { fh = new FileHandler("Repository.log",true); LOGGER.addHandler(fh); } catch (SecurityException e) { LOGGER.severe("Impossible to open FileHandler"); }catch (IOException e) { LOGGER.severe("Impossible to open FileHandler"); };
    }

    public String getID() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        return ID;
    }

    public void setID(String iD) {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +iD.toString());
        ID = iD;
    }

    public List<Product> getProducts() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        return products;
    }

    public void setProducts(List<Product> products) {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +products.toString());
        this.products = products;
    }

    public Product fetchProduct(String ID) throws ProductNotFoundException {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +ID.toString());
        for (Product product : products) {
            if (product.getID().equals(ID)) {
                return product;
            }
        }
        throw new ProductNotFoundException("No product with the provided ID exists.");
    }

    public Product addProduct(Product product) throws ProductAlreadyExisteException {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +product.toString());
        try {
            this.fetchProduct(product.getID());
            throw new ProductAlreadyExisteException("A product with the same ID already exists.");
        } catch (ProductNotFoundException e) {
            products.add(product);
            return product;
        }
    }

    public Product deleteProduct(String ID) throws ProductNotFoundException {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +ID.toString());
        Product p = this.fetchProduct(ID);
        products.remove(p);
        return p;
    }

    public Product updateProduct(Product product) throws ProductNotFoundException, ProductAlreadyExisteException {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +product.toString());
        this.deleteProduct(product.getID());
        this.addProduct(product);
        return product;
    }

    public void diplayProducts() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        StringBuilder sb = new StringBuilder();
        sb.append("Printing products database :");
        sb.append('\n');
        for (Product product : products) {
            sb.append(product);
            sb.append('\n');
        }
        sb.append("End of printing.");
        System.out.println(sb.toString());
    }

    @Override
    public String toString() {
        LOGGER.info("no parameters given");
        return ((("Repository [ID=" + ID) + ", products=") + products) + "]";
    }
}