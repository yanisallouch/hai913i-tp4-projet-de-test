# HAI913I TP4 Projet de test

A simple application (with a CLI or a GUI) that allows you to :

1. create a user with an ID, name, age, email, and password.
2. provide a user with a menu through which (s)he can :
	- display products in a repository, where every product has an ID, a
	name, a price, and a expiration date.
	- fetch a product by its ID (if no product with the provided ID exists,
	an exception must be thrown).
	- add a new product (if a product with the same ID already exists, an
	exception must be thrown).
	- delete a product by its ID (if no product with the provided ID exists,
	an exception must be thrown).
	- update a product’s info (if no product with the provided ID exists,
	an exception must be thrown) .

![simple_application.drawio.png](./simple_application.drawio.png)

##	Script

Le script [`run_spooned_app.sh`](./scripts/run_spooned_app.sh), permet d'automatiser l'éxécution de l'application instrumenté pour générer différents scénario (nécéssité une intéraction dans la console avec l'application instrumenté). Une fois tous les scénarios d'éxécutions appliquer. Ne garder que le fichier `Repository.log`.

## What is next ?

Une fois le projet de test éxécuté et compris, vous pouvez consulter la suite dans ce [projet](https://gitlab.com/yanisallouch/hai913i-tp5-spoon-logging) dévéloppé en continuité pour nos besoins permettant d'instrumenter le code par le code via [Spoon](https://spoon.gforge.inria.fr/) dans un objectif de tracing par logging!